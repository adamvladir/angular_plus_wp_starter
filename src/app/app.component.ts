import {Component} from "@angular/core";

/**
 * @description App root component
 */
@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.less']
})
export class AppComponent{

    /**
     * Some text to display
     * @type {string}
     */
    text: string = 'Angular+ WebPack Starter!!!';

}