var webpack = require('webpack'),
    webpackMerge = require('webpack-merge'),
    TypeDocWebpackPlugin = require('typedoc-webpack-plugin'),
    helper = require('./helper'),
    commonConfig = require('./webpack.common.js');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(commonConfig, {

    output: {
        filename: '[name].min.js'
    },

    plugins: [
        new webpack.DefinePlugin({
            'process.env': {
                'ENV': JSON.stringify(ENV)
            }
        }),
        new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
            mangle: {
                keep_fnames: true
            }
        }),
        new webpack.LoaderOptionsPlugin({
            htmlLoader:{
                minimize: false
            }
        }),
        new TypeDocWebpackPlugin({
            mode: "modules",
            theme: "default",
            ignoreCompilerErrors: true,
            experimentalDecorators: true,
            emitDecoratorMetadata: true,
            target: "ES5",
            moduleResolution: "node",
            preserveConstEnums: true,
            stripInternal: true,
            excludeExternals: true,
            exclude: "**/*.spec.ts",
            suppressExcessPropertyErrors: true,
            suppressImplicitAnyIndexErrors: true,
            module: "commonjs",
            out: helper.root('bundle', 'docs')
        }, helper.root('src', 'app'))
    ]
});