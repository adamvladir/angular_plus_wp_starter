var webpack = require('webpack'),
    helper = require('./helper');

module.exports = {
    devtool: 'inline-source-map',

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
            {
                test: /\.ts$/,
                use:[
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            configFilename: helper.root('src', 'tsconfig.json')
                        }
                    },
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'null-loader'
            },
            {
                test: /\.css$/,
                include: helper.root('src', 'app'),
                loader: 'raw-loader'
            },
            {
                test: /\.less$/,
                include: helper.root('src'),
                use: ['raw-loader', 'less-loader']
            }
        ]
    },

    plugins: [
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            helper.root('src'), // location of your src
            {} // a map of your routes
        )
    ]
};