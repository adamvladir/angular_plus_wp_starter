var webpack = require('webpack'),
    HtmlWebPackPlugin = require('html-webpack-plugin'),
    helper = require('./helper');

module.exports = {

    entry: {
        'polyfills': './src/polyfills.ts',
        'vendor': './src/vendor.ts',
        'app': './src/main.ts'
    },

    output: {
        path: helper.deployPath('bundle'),
        publicPath: '/',
        filename: '[name].js',
        chunkFilename: '[id].chunk.js'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    bail: true,
    profile: true,

    module: {
        rules: [
            {
                test: /\.ts$/,
                use:[
                    {
                        loader: 'awesome-typescript-loader',
                        options: {
                            configFilename: helper.root('src', 'tsconfig.json')
                        }
                    },
                    'angular2-template-loader'
                ]
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
                loader: 'file-loader?name=assets/[name].[hash].[ext]'
            },
            {
                test: /\.css$/,
                include: helper.root('src'),
                loader: 'raw-loader'
            },
            {
                test: /\.less$/,
                include: helper.root('src'),
                use: ['raw-loader', 'less-loader']
            }
        ]
    },

    plugins: [
        // Workaround for angular/angular#11580
        new webpack.ContextReplacementPlugin(
            // The (\\|\/) piece accounts for path separators in *nix and Windows
            /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
            helper.root('src'), // location of your src
            {} // a map of your routes
        ),
        new webpack.optimize.CommonsChunkPlugin({
            name: ['app', 'vendor', 'polyfills']
        }),

        new HtmlWebPackPlugin({
            template: 'src/index.html'
        }),

        new webpack.NoEmitOnErrorsPlugin()
    ]
};
